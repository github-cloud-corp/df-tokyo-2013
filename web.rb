require 'sinatra'
require 'json'

$stdout.sync = true
enable :sessions

get '/' do
  erb :index 
end

helpers do
  def log2td(ev)
    if not ev[:time]
      ev[:time] = Time.now.to_i
    end
    ev[:session_id] = session[:session_id]
    puts "@[df_jp_2013.visitor_action] #{ev.to_json}"
  end
end

before do
  log2td(:url_path => request.path, :ev_type => "start_page")
end

after do
  log2td(:url_path => request.path, :ev_type => "end_page")
end
